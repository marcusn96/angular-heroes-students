V-as ruga sa incepeti cu un mic tutorial legat de Angular.

Se numeste Angular Heroes si va las link-ul aici:
https://angular.io/tutorial

In partea stanga a paginii aveti meniul de navigatie, care contine si capitolele pe care le veti parcurge.
Pentru acest tutorial o sa folositi VISUAL STUDIO CODE, cu Terminalul built-in (in partea de sus: Terminal -> New Terminal, aici veti rula comenzile)

V-as ruga sa va faceti un branch separat si sa faceti commit si push la cod acolo, de recomandat, dupa fiecare capitol pe care il finalizati, nu lasati sa se adune mult cod ne-push-uit :)
Pentru orice intrebare, va ajut cu mare placere.
Spor !